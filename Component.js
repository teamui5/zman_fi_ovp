(function () {
	"use strict";
	/*global sap, jQuery */

	/**
	 * @fileOverview Application component to display information on entities from the GWSAMPLE_BASIC
	 *   OData service.
	 * @version @version@
	 */
	jQuery.sap.declare("fr.manessens.OVP_Finance.ZFI_OVP_V2.Component");

	jQuery.sap.require("sap.ovp.app.Component");

	sap.ovp.app.Component.extend("fr.manessens.OVP_Finance.ZFI_OVP_V2.Component", {
		metadata: {
			manifest: "json"
		}
	});
}());